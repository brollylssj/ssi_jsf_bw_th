package com.ssi.jsf;

import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ValueChangeEvent;
import javax.faces.event.ValueChangeListener;

public class ColorChangeListener implements ValueChangeListener {
   @Override
   public void processValueChange(ValueChangeEvent event)
      throws AbortProcessingException {
      //access country bean directly
      ColorUser user = (ColorUser) FacesContext.getCurrentInstance().
         getExternalContext().getSessionMap().get("colorUser");

      user.setChangedColor(event.getNewValue().toString());
   }
}
