package com.ssi.jsf;

import java.util.List;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.naming.NamingException;

import com.fourspaces.couchdb.Document;
import com.fourspaces.couchdb.ViewResults;

@ManagedBean(name = "user", eager = true)
@RequestScoped
public class User implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String firstName;
	private String lastName;
	private String email;
	private String password;
	private String sex;
	private String carrer;
	private String[] role;
	private String message;
	
	private String timeZone;
	
	
	
	
	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	private String selectedCarrer;

	public String getSelectedCarrer() {
		return selectedCarrer;
	}

	public void setSelectedCarrer(String selectedCarrer) {
		this.selectedCarrer = selectedCarrer;
	}

	CouchDB couchDB = new CouchDB();

	public String[] getRole() {
		return role;
	}

	public void setRole(String[] role) {
		this.role = role;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getCarrer() {
		return carrer;
	}

	public void setCarrer(String carrer) {
		this.carrer = carrer;
	}

	public User() {

	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String name) {
		this.firstName = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String add() {
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		Map<String, String> parameterMap = (Map<String, String>) ec.getRequestParameterMap();

		HashMap<String, String> properties = new HashMap<String, String>();
		properties.put(couchDB.KEY_FIRST_NAME, parameterMap.get("registerForm:fname"));
		properties.put(couchDB.KEY_LAST_NAME, parameterMap.get("registerForm:lname"));
		properties.put(couchDB.KEY_EMAIL, parameterMap.get("registerForm:email"));
		properties.put(couchDB.KEY_PASSWORD, parameterMap.get("registerForm:psw"));
		properties.put(couchDB.KEY_SEX, parameterMap.get("registerForm:sex_id"));
		properties.put(couchDB.KEY_CARRER, parameterMap.get("registerForm:carrer_id"));
		properties.put(couchDB.KEY_ROLE, parameterMap.get("registerForm:role_id"));

		try {
			couchDB.save(properties);
		} catch (Exception e) {
			System.out.println("nie wykonano zapisu do CouchDB");
			return "unsuccess";
		}
		System.out.println("User zapisany do bazy");
		return "success";

	}

	public String login() {
		String inputName = this.firstName;
		String inputPassword = this.password;
		System.out.println(inputName +" " + inputPassword);
		ViewResults couchViewResults = couchDB.getDocuments();
		List<Document> documents = couchViewResults.getResults();
		for (Document couchDocument : documents) {
			String id = couchDocument.getJSONObject().getString("id");
			Document row = couchDB.getCouchDB().getDocument(id);
			if (row.containsKey(couchDB.KEY_FIRST_NAME)) {
				if (row.get(couchDB.KEY_FIRST_NAME).equals(inputName)) {
					if (row.containsKey(couchDB.KEY_PASSWORD)) {
						if (row.get(couchDB.KEY_PASSWORD).equals(inputPassword)) {
							System.out.println("Zalogowano");
							return "success";
						}
					}
				}
			}
			
		} 
		
		return "unsuccess";

	}

	public void logout() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		FacesContext.getCurrentInstance().getApplication().getNavigationHandler()
				.handleNavigation(FacesContext.getCurrentInstance(), null, "/index.xhtml");
	}


	public String getWelcomeMessage() {
		if (message != null)
			return "Your message: "+ message;
		return "";
	}

	public String getTime() {
		DateTimeFormatter dateFormat =
			    DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		
			LocalDateTime date = LocalDateTime.now();
			
		if (timeZone != null)
			return dateFormat.format(date.plusHours(Integer.parseInt(timeZone))).toString();
		return "";
	}
	
	
	
}
