package com.ssi.jsf;

import java.util.Map;
import com.fourspaces.couchdb.Database;
import com.fourspaces.couchdb.Document;
import com.fourspaces.couchdb.Session;
import com.fourspaces.couchdb.ViewResults;

class CouchDB {
	
	Session dataBaseSession = new Session("localhost", 5984);
	Database couchDB = dataBaseSession.getDatabase("ssi_jsf");
	
	

 public Database getCouchDB() {
		return couchDB;
	}

	public void setCouchDB(Database couchDB) {
		this.couchDB = couchDB;
	}

/*These are the keys of student document in couch db*/
 public final String KEY_FIRST_NAME = "firstName";
 public final String KEY_LAST_NAME = "lastName";
 public final String KEY_EMAIL= "email";
 public final String KEY_PASSWORD ="password";
 public final String KEY_SEX = "sex";
 public final String KEY_CARRER= "carrer";
 public final String KEY_ROLE= "role";
 
 
 public void save(Map<String , String> properties)
 {
	  Document newdoc = new Document();
	  newdoc.putAll(properties);
	  couchDB.saveDocument(newdoc);  
 }
 
 public ViewResults getDocuments()
 {
	 ViewResults couchViewResults = couchDB.getAllDocuments();
	 return couchViewResults;
 }
}