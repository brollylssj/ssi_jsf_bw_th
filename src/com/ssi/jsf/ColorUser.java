package com.ssi.jsf;

import java.io.Serializable;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

@ManagedBean(name = "colorUser")
@SessionScoped
public class ColorUser implements Serializable{
	
	
	private String changedColor = "Blue";
	
	public String getChangedColor() {
		return changedColor;
	}


	public void setChangedColor(String changedColor) {
		this.changedColor = changedColor;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public ColorUser() {
	}

	@ManagedProperty(value="#{colorName}")
	private String colorName = "";

	
	public String getColorName() {
		return colorName;
	}


	public void setColorName(String colorName) {
		this.colorName = colorName;
	}


	public String selectedColor()
	{
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
    	Map<String, String> parameterMap = (Map<String, String>) ec.getRequestParameterMap();
    	String color = parameterMap.get("colorForm:color_id");
    	System.out.println(color);
    	return "color-success";
	}
	
	public void changeColorListener(ValueChangeEvent e){
		changedColor = e.getNewValue().toString();
		
	}
	
	
}
